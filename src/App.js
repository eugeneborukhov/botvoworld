/**
 * Created by LzxHahaha on 2016/10/6. https://github.com/LzxHahaha/react-dinosaur-game
 * Modified by Brivo (Eugene Borukhov,Catherine Baird, & Ethan Allred), December 2021
 * Music and sounds from https://www.zapsplat.com/
 * Graphics by Catherine
 */

import React from "react";
import { Grid, Typography } from "@material-ui/core";
import ceilingSrc from "./img/cameras.png";
import groundSrc from "./img/ground1.png";
import playerImageSrc from "./img/botvo.png";
import playerLeftImageSrc from "./img/botvo-left.png";
import playerRightImageSrc from "./img/botvo-right.png";
import playerDieImageSrc from "./img/botvo-die.png";
import closedDoorImageSrc from "./img/door-closed.png";
import openDoorImageSrc from "./img/door-open.png";
import botvoAwesome from "./img/botvo-awesome.jpg";
import pingSoundSrc from "./audio/zapsplat-ping.mp3";
import crashSoundSrc from "./audio/zapsplat-crash.mp3";
import backgroundSongSrc from "./audio/zapsplat-piano.mp3";

const STATUS = {
  STOP: "STOP",
  START: "START",
  PAUSE: "PAUSE",
  OVER: "OVER",
};

const JUMP_DELTA = 15;
const JUMP_MAX_HEIGHT = 60;

export default class App extends React.Component {
  constructor(props) {
    super(props);

    let imageLoadCount = 0;
    let onImageLoaded = () => {
      ++imageLoadCount;
      if (imageLoadCount === 3) {
        this.__draw();
      }
    };

    let skyImage = new Image();
    let groundImage = new Image();
    let playerImage = new Image();
    let playerLeftImage = new Image();
    let playerRightImage = new Image();
    let playerDieImage = new Image();
    let closedDoorImage = new Image();
    let openDoorImage = new Image();
    let pingSound = new Audio();
    let crashSound = new Audio();
    let backgroundMusic = new Audio();

    skyImage.onload = onImageLoaded;
    groundImage.onload = onImageLoaded;
    playerImage.onload = onImageLoaded;

    skyImage.src = ceilingSrc;
    groundImage.src = groundSrc;
    playerImage.src = playerImageSrc;
    playerLeftImage.src = playerLeftImageSrc;
    playerRightImage.src = playerRightImageSrc;
    playerDieImage.src = playerDieImageSrc;
    closedDoorImage.src = closedDoorImageSrc;
    openDoorImage.src = openDoorImageSrc;
    //audio:
    pingSound.src = pingSoundSrc;
    crashSound.src = crashSoundSrc;
    backgroundMusic.src = backgroundSongSrc;

    this.options = {
      fps: 60,
      skySpeed: 60,
      groundSpeed: 200,
      skyImage: skyImage,
      groundImage: groundImage,
      playerImage: [
        playerImage,
        playerLeftImage,
        playerRightImage,
        playerDieImage,
      ],
      closedDoorImage: closedDoorImage,
      openDoorImage: openDoorImage,
      pingSound: pingSound, //might be able to take the sounds out of options
      crashSound: crashSound,
      backgroundMusic: backgroundMusic,
      skyOffset: 0,
      groundOffset: 0,
      ...this.props.options,
    };

    this.status = STATUS.STOP;
    this.timer = null;
    this.score = 0;
    this.highScore = window.localStorage
      ? window.localStorage["highScore"] || 0
      : 0;
    this.jumpHeight = 0;
    this.jumpDelta = 0;
    this.obstaclesBase = 1;
    this.obstacles = this.__obstaclesGenerate();
    this.currentDistance = 0;
    this.playerStatus = 0;
    this.state = {
      status: STATUS.STOP,
      seconds: 20,
      currentEvent: null,
    };
  }

  componentDidMount() {
    const onSpacePress = () => {
      switch (this.status) {
        case STATUS.STOP:
          this.start();
          break;
        case STATUS.START:
          this.jump();
          break;
        case STATUS.OVER:
          this.restart();
          break;
      }
    };

    window.onkeypress = function (e) {
      if (e.key === " ") {
        onSpacePress();
      }
    };
    this.canvas.parentNode.onclick = onSpacePress;

    window.onblur = this.pause;
    window.onfocus = this.goOn;
  }

  componentWillUnmount() {
    window.onblur = null;
    window.onfocus = null;
  }

  __draw() {
    if (!this.canvas) {
      return;
    }

    const { options } = this;

    let level = Math.min(200, Math.floor(this.score / 100));
    let groundSpeed = (options.groundSpeed + level) / options.fps;
    let skySpeed = options.skySpeed / options.fps;
    let obstacleWidth = options.closedDoorImage.width;
    let playerWidth = options.playerImage[0].width;
    let playerHeight = options.playerImage[0].height;

    const ctx = this.canvas.getContext("2d");
    const { width, height } = this.canvas;

    ctx.clearRect(0, 0, width, height);
    ctx.save();

    this.options.skyOffset =
      this.options.skyOffset < width
        ? this.options.skyOffset + skySpeed
        : this.options.skyOffset - width;
    ctx.translate(-this.options.skyOffset, 0);
    ctx.drawImage(this.options.skyImage, 0, 0);
    ctx.drawImage(this.options.skyImage, this.options.skyImage.width, 0);

    this.options.groundOffset =
      this.options.groundOffset < width
        ? this.options.groundOffset + groundSpeed
        : this.options.groundOffset - width;
    ctx.translate(this.options.skyOffset - this.options.groundOffset, 0);
    ctx.drawImage(this.options.groundImage, 0, 76);
    ctx.drawImage(this.options.groundImage, this.options.groundImage.width, 76);

    ctx.translate(this.options.groundOffset, 0);
    ctx.drawImage(
      this.options.playerImage[this.playerStatus],
      80,
      64 - this.jumpHeight
    );
    this.jumpHeight = this.jumpHeight + this.jumpDelta;
    if (this.jumpHeight <= 1) {
      this.jumpHeight = 0;
      this.jumpDelta = 0;
    } else if (this.jumpHeight < JUMP_MAX_HEIGHT && this.jumpDelta > 0) {
      this.jumpDelta =
        this.jumpHeight * this.jumpHeight * 0.001033 -
        this.jumpHeight * 0.137 +
        5;
    } else if (this.jumpHeight < JUMP_MAX_HEIGHT && this.jumpDelta < 0) {
      // jumpDelta = (jumpHeight * jumpHeight) * 0.00023 - jumpHeight * 0.03 - 4;
    } else if (this.jumpHeight >= JUMP_MAX_HEIGHT) {
      this.jumpDelta = -JUMP_DELTA / 2.7;
    }

    let scoreText =
      (this.status === STATUS.OVER ? "SCORE:  " : "SCORE: ") +
      Math.floor(this.score);
    ctx.font = "Bold 18px Arial";
    ctx.textAlign = "left";
    ctx.fillStyle = "#7796e1";
    ctx.fillText(scoreText, 30, height - 100);
    if (this.status === STATUS.START) {
      // this.score += 0.5;
      if (this.score > this.highScore) {
        this.highScore = this.score;
        window.localStorage["highScore"] = this.score;
      }
      this.currentDistance += groundSpeed;

      //this seemed to fix the running botvo bug, keep a side-eye for unwanted effects
      if (Math.floor(Date.now() / 20) % 4 === 0) {
        this.playerStatus = (this.playerStatus + 1) % 3;
      }
    }
    if (this.highScore) {
      ctx.textAlign = "right";
      ctx.fillText(
        "HIGH SCORE: " + Math.floor(this.highScore),
        width - 30,
        height - 100
      );
    }

    ctx.font = "Bold 12px Arial";
    ctx.textAlign = "right";
    ctx.fillText(
      "{ music + sound effects from zapsplat.com }",
      width - 30,
      height - 10
    );

    let pop = 0;
    for (let i = 0; i < this.obstacles.length; ++i) {
      if (this.currentDistance >= this.obstacles[i].distance) {
        let offset =
          width -
          (this.currentDistance - this.obstacles[i].distance + groundSpeed);
        if (offset > 0) {
          //the obstacle we draw depends on obstacles[i].type
          if (this.obstacles[i].type === "open") {
            ctx.drawImage(options.openDoorImage, offset, 60);
          } else {
            ctx.drawImage(options.closedDoorImage, offset, 60);
          }
        } else {
          ++pop;
        }
      } else {
        break;
      }
    }
    for (let i = 0; i < pop; ++i) {
      this.obstacles.shift();
    }
    if (this.obstacles.length < 5) {
      this.obstacles = this.obstacles.concat(this.__obstaclesGenerate());
    }

    let firstOffset =
      width - (this.currentDistance - this.obstacles[0].distance + groundSpeed);
    if (
      90 - obstacleWidth < firstOffset &&
      firstOffset < 60 + playerWidth &&
      64 - this.jumpHeight + playerHeight > 84
    ) {
      //botvo encounters an object
      if (this.obstacles[0].type === "open") {
        this.__playPingSound();
        this.score += 1;
        // const validAccessText = "VALID ACCESS!";
        ctx.font = "italic Bold 50px Arial";
        ctx.textAlign = "center";
        ctx.fillStyle = "#228B22";
        this.setState({ currentEvent: { name: "Valid Access", open: true } });
        // ctx.fillText(validAccessText, width / 2, 190);
      } else {
        this.__playCrashSound();
        this.score -= 1;
        // const failedAccessText = "FAILED ACCESS!";
        ctx.font = "italic Bold 50px Arial";
        ctx.textAlign = "center";
        ctx.fillStyle = "#C41E3A";
        this.setState({ currentEvent: { name: "Failed Access", open: false } });
        // ctx.fillText(failedAccessText, width / 2, 190);
      }
    }

    ctx.restore();
  }

  __playPingSound() {
    this.options.pingSound.play();
    setTimeout(() => {
      this.options.pingSound.pause();
      this.options.pingSound.currentTime = 0;
    }, 275);
  }

  __playCrashSound() {
    this.options.crashSound.play();
    setTimeout(() => {
      this.options.crashSound.pause();
      this.options.crashSound.currentTime = 0;
    }, 275);
  }

  __playBackgroundMusic() {
    this.options.backgroundMusic.play();
    setTimeout(() => {
      this.options.backgroundMusic.pause();
      this.options.backgroundMusic.currentTime = 0;
    }, 20500);
  }

  __obstaclesGenerate() {
    let res = [];
    for (let i = 0; i < 10; ++i) {
      let random = Math.floor(Math.random() * 100) % 60;
      random = ((Math.random() * 10) % 2 === 0 ? 1 : -1) * random;
      //get 0 or 1
      const chooseType = Math.floor(Math.random() * (1 - 0 + 1));
      const doorType = chooseType ? "open" : "closed";
      res.push({
        distance: random + this.obstaclesBase * 200 - 3000,
        type: doorType,
      });
      ++this.obstaclesBase;
    }
    return res;
  }

  __setTimer() {
    this.timer = setInterval(() => this.__draw(), 1000 / this.options.fps);
    setTimeout(() => this.stop(), 20000);
  }

  __clearTimer() {
    if (this.timer) {
      clearInterval(this.timer);
      this.timer = null;
    }
  }

  __clear() {
    this.score = 0;
    this.jumpHeight = 0;
    this.currentDistance = 0;
    this.obstacles = [];
    this.obstaclesBase = 1;
    this.playerStatus = 0;
  }

  start = () => {
    if (this.status === STATUS.START) {
      return;
    }

    this.status = STATUS.START;
    this.setState({ status: STATUS.START });
    const gameInterval = setInterval(() => {
      this.setState({
        seconds:
          this.state.seconds > 0 ? this.state.seconds - 1 : this.state.seconds,
      });
    }, 1000);
    setTimeout(() => clearInterval(gameInterval), 20000);
    this.__setTimer();
    this.__playBackgroundMusic();
    this.jump();
  };

  pause = () => {
    if (this.status === STATUS.START) {
      this.status = STATUS.PAUSE;
      this.setState({ status: STATUS.PAUSE });

      this.__clearTimer();
    }
  };

  goOn = () => {
    if (this.status === STATUS.PAUSE) {
      this.status = STATUS.START;
      this.setState({ status: STATUS.START });

      this.__setTimer();
    }
  };

  stop = () => {
    if (this.status === STATUS.OVER) {
      return;
    }
    this.status = STATUS.OVER;
    this.setState({ status: STATUS.OVER, seconds: 20 });
    this.playerStatus = 3;
    this.__clearTimer();
    this.__draw();
    this.__clear();
  };

  restart = () => {
    this.obstacles = this.__obstaclesGenerate();
    this.start();
  };

  jump = () => {
    if (this.jumpHeight > 2) {
      return;
    }
    this.jumpDelta = JUMP_DELTA;
    this.jumpHeight = JUMP_DELTA;
  };

  render() {
    console.log("this.state.status", this.state.status);
    return (
      <>
        {this.state.status === STATUS.STOP ? (
          <>
            <div
              style={{
                fontSize: "72px",
                margin: "auto",
                textAlign: "center",
                color: "#7796e1",
              }}
            >
              Botvo World
            </div>
            <div
              class="runner-container"
              style={{
                width: "600px",
                margin: "auto",
                transform: `scale(${window.innerWidth / 600}) translateY(10px)`,
                transformOrigin: "top center",
                height: "150px",
                textAlign: "center",
                color: "#7796e1",
              }}
            >
              <img src={botvoAwesome} height="172" width="260" />
              <div>Press space to play.</div>
              <div>Go through open doors and jump over closed doors.</div>
              <canvas
                id="canvas"
                ref={(ref) => (this.canvas = ref)}
                height={0}
                width={0}
              />
            </div>
          </>
        ) : (
          <div
            class="runner-container"
            style={{
              width: "600px",
              left: 0,
              right: 0,
              margin: "auto",
              transform: `scale(${window.innerWidth / 600}) translateY(20px)`,
              transformOrigin: "top center",
              height: "150px",
            }}
          >
            <Grid
              container
              justifyContent="space-around"
              style={{ marginBottom: "20px" }}
            >
              <Grid
                item
                style={{
                  border: `4px ${
                    this.state.status === STATUS.OVER ? "#f44336" : "#7796e1"
                  } solid`,
                  borderRadius: "4px",
                  padding: "4px",

                  textAlign: "center",
                }}
              >
                <Typography variant="h6" color="primary">
                  Event Tracker
                </Typography>
                {this.state.currentEvent && (
                  <div>
                    <Typography
                      variant="body2"
                      color={
                        this.state.currentEvent.open ? "textPrimary" : "error"
                      }
                    >
                      {this.state.currentEvent.name}
                    </Typography>
                  </div>
                )}
              </Grid>
              {this.state.status === STATUS.OVER && (
                <Grid item style={{ marginTop: "30px" }}>
                  <Typography color="error">Lockdown Activated</Typography>
                </Grid>
              )}
              <Grid
                item
                style={{
                  border: `4px ${
                    this.state.status === STATUS.OVER ? "#f44336" : "#7796e1"
                  } solid`,
                  borderRadius: "4px",

                  height: "90px",
                  padding: "4px",
                  textAlign: "center",
                }}
              >
                <Typography variant="h6" color="primary">
                  Snapshot Log
                </Typography>
                {this.state.currentEvent && (
                  <div>
                    {this.state.currentEvent.open ? (
                      <img src={playerImageSrc} />
                    ) : (
                      <img src={playerDieImageSrc} />
                    )}
                  </div>
                )}
              </Grid>
            </Grid>
            <div style={{ color: "#7796e1", textAlign: "center" }}>
              {this.state.status === STATUS.OVER ? (
                <div>Press space to play again</div>
              ) : (
                <div>{this.state.seconds}</div>
              )}
            </div>
            <canvas
              id="canvas"
              ref={(ref) => (this.canvas = ref)}
              height={300}
              width={1200}
              style={{ width: "600px", height: "150px" }}
            />
          </div>
        )}
      </>
    );
  }
}
